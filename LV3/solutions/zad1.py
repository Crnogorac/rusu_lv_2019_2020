# -*- coding: utf-8 -*-
"""
Created on Wed Dec  8 12:43:45 2021

@author: Bernarda
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars = pd.read_csv('mtcars.csv')

#1
autiMPG = mtcars.sort_values("mpg")
print("5 auta s najvećom potršnjom: ", autiMPG.head(5))

#2
autiCilindri = mtcars.sort_values("mpg")
print("3 auta s 8 cilindra koji imaju najmanju potrošnju: ", autiCilindri[autiCilindri.cyl == 8].tail(3))

#3
avgCilindri = mtcars[mtcars.cyl == 6].groupby("cyl")
print("Srednja potrošnja auta sa 6 cilindra: ", avgCilindri.mpg.mean())

#4
avg4Cilindra = mtcars[(mtcars.cyl == 4) & (mtcars.wt = 2.000)].groupby("cyl")
print("Srednja potrošnja auta s 4 cilindra i masom između 2000 i 2200 lbs: ", avg4Cilindra.mpg.mean())

#5
rucniMjenjac = mtcars[mtcars.am == 1]
automatskiMjenjac = mtcars[mtcars.am == 0]
print("Broj auta s rucnim mjenjacem: ", len(rucniMjenjac))
print("\nBroj auta s automatskim mjenjacem: ", len(automatskiMjenjac))

#6
amSnaga = mtcars[(mtcars.am == 0) & (mtcars.hp > 100)]
print("Broj auta s automatskim mjenjacem i snagom preko 100 hp: ", len(amSnaga))

#7
lbsToKg=mtcars['wt']*0.45359237
print("Masa svakog auta u kilogramima:\n", lbsToKg)