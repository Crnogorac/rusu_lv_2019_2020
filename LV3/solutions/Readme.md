### mtcars.csv

Motor Trend Car Road Tests

The data was extracted from the 1974 Motor Trend US magazine, and comprises fuel consumption and 10 aspects of automobile design and performance for 32 automobiles (1973–74 models)
* mpg - Miles/(US) gallon
* cyl - Number of cylinders
* disp - Displacement (cu.in.)
* hp - Gross horsepower
* drat - Rear axle ratio
* wt - Weight (lb/1000)
* qsec - 1/4 mile time
* vs - V/S
* am - Transmission (0 = automatic, 1 = manual)
* gear - Number of forward gears
* carb - Number of carburetors

Source: R built in dataset



### AirQualityRH.py (preimenovana u zad3)

Skripta za dohvaćanje podataka o kvaliteti zraka pomocu REST API

-------------------------------------------------------------------
1.ZADATAK: potrebno je grupirati određene podatke po zahtjevu u zadatku. Koriste se cilindri, mjenjaci, potrošnja, snaga te masa automobila. Ovisno o zahtjevu u zadatku, filtrirati i prikazati određene podatke.
2.ZADATAK: potrebno je grafički prikazati određene skupove podataka koje se traže u zadatku putem plot.bar()
3.ZADATAK: potrebno je nadopuniti skriptu AirQualityRH (zad3) kako bi dobili određene skupove podataka tražene u zadatku barplota/boxplota, ovisno o zadatku. 
