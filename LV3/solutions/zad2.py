# -*- coding: utf-8 -*-
"""
Created on Wed Dec  8 12:46:22 2021

@author: Bernarda
"""

import pandas as pd

mtcars = pd.read_csv('mtcars.csv')

#1
autiCilindri = mtcars[['mpg','cyl']].groupby('cyl')
autiCilindri = autiCilindri.mean()
autiCilindri.plot.bar(rot=0, color ="pink")

#2
autiTezina = mtcars[['wt', "cyl"]]
autiTezina.boxplot(by="cyl")

#3
mjenjacPotrosnja = mtcars[['am', 'mpg']].groupby('am')
mjenjacPotrosnja = mjenjacPotrosnja.mean()
mjenjacPotrosnja.plot.bar(rot=0, color="violet")

#4
autiUbrzanje = mtcars[['hp','qsec','am']].groupby('am')
autiUbrzanje = autiUbrzanje.mean()
autiUbrzanje.plot.bar(rot=0)