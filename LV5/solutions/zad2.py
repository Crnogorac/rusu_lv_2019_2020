# -*- coding: utf-8 -*-
"""
Created on Mon Dec 13 22:35:06 2021

@author: Bernarda
"""

from sklearn import datasets
import numpy as np
from sklearn import cluster
import matplotlib.pyplot as plt

def generate_data(n_samples, flagc):
    
    if flagc == 1:
        random_state = 365
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        
    elif flagc == 2:
        random_state = 148
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)
        
    elif flagc == 3:
        random_state = 148
        X, y = datasets.make_blobs(n_samples=n_samples,
                          cluster_std=[[1.0, 2.5], [0.5, 3.0], [1.5, 2.5]],
                          random_state=random_state)

    elif flagc == 4:
        X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
        
    elif flagc == 5:
        X, y = datasets.make_moons(n_samples=n_samples, noise=.05)
    
    else:
        X = []
        
    return X

J=[]
n_samples=500
flagc=2

X=generate_data(n_samples, flagc)

optimalCenterAmount=0
for iter in range(1,20):
    kmeans=cluster.KMeans(n_clusters=iter, random_state=77).fit(X)
    y_kmeans = kmeans.predict(X)
    if(len(J)>0):
        if(kmeans.inertia_<min(J)*0.8):
            optimalCenterAmount=iter
    J.append(kmeans.inertia_)

kmeans=cluster.KMeans(n_clusters=optimalCenterAmount, random_state=77).fit(X)
plt.scatter(X[:, 0], X[:, 1], c=y_kmeans, s=50)

centers = kmeans.cluster_centers_

plt.scatter(centers[:, 0], centers[:, 1], c='red', s=200, alpha=1, marker="*");

plt.figure()
plt.plot(np.arange(19)+1,J,label="f")
plt.xlabel("Number of centroids")
plt.ylabel("Error rate")
plt.legend()
plt.show()

print("Optimal amount of centers is ",optimalCenterAmount)
