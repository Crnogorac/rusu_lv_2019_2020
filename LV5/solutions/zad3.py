# -*- coding: utf-8 -*-
"""
Created on Mon Dec 13 22:42:10 2021

@author: Bernarda
"""

from sklearn import datasets
import numpy as np
from scipy.cluster.hierarchy import dendrogram, linkage 
import matplotlib.pyplot as plt

def generate_data(n_samples, flagc):
    
    if flagc == 1:
        random_state = 365
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        
    elif flagc == 2:
        random_state = 148
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)
        
    elif flagc == 3:
        random_state = 148
        X, y = datasets.make_blobs(n_samples=n_samples,
                          cluster_std=[[1.0, 2.5], [0.5, 3.0], [1.5, 2.5]],
                          random_state=random_state)

    elif flagc == 4:
        X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
        
    elif flagc == 5:
        X, y = datasets.make_moons(n_samples=n_samples, noise=.05)
    
    else:
        X = []
        
    return X



n_samples = 50
flagc = 1
X = generate_data(n_samples, flagc)

plt.figure()
plt.scatter(X[:, 0], X[:, 1], c='violet', s=50)
plt.show()
Y=linkage(X,method='weighted',metric='euclidean')
dendrogram = dendrogram(Y)
