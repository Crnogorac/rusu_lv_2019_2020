# -*- coding: utf-8 -*-
"""
Created on Tue Nov 30 18:24:17 2021

@author: Bernarda
"""

import numpy as np
from numpy.random import default_rng
import matplotlib.pyplot as plt

def makeGraph(dataArray, graphColor):
    plt.hist(dataArray, color=graphColor, density=True, edgecolor='k')
    plt.axvline(dataArray.mean(), color='k', linestyle='dashed', linewidth=1.5)
    min_ylim, max_ylim = plt.ylim()
    plt.text(dataArray.mean()*1.005, max_ylim*0.9, 'Mean: {:.2f}'.format(dataArray.mean()))

def avgHeight(genders, number):
    if number == 1:
        avgHeights = np.count_nonzero(genders == 1)
    else:
        avgHeights = np.count_nonzero(genders == 0)
    return avgHeights


rng = default_rng()

genders = rng.integers(2, size=(1, 100))
maleHeight = np.random.normal(180, 7, avgHeight(genders, 1))
femaleHeight = np.random.normal(167, 7, avgHeight(genders, 0))

plt.subplot(1,2,1)
plt.title("Male height distribution")
makeGraph(maleHeight,'b')

plt.subplot(1,2,2)
plt.title("Female height distribution")
makeGraph(femaleHeight,'r')