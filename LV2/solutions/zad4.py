# -*- coding: utf-8 -*-
"""
Created on Tue Nov 30 18:31:06 2021

@author: Bernarda
"""

import numpy as np
from numpy.random import default_rng
import matplotlib.pyplot as plt

def makeGraph(dataArray,graphColor):
    plt.hist(dataArray, color=graphColor, density=True, edgecolor='k',bins=np.arange(2, 14)/2)
    plt.axvline(dataArray.mean(), color='k', linestyle='dashed', linewidth=1.5)
    min_ylim, max_ylim = plt.ylim()
    plt.text(dataArray.mean()*1.005, max_ylim*0.9, 'Mean: {:.2f}'.format(dataArray.mean()))
    

rng = default_rng()

dieRolls = rng.integers(low=1, high=7, size=100)

plt.figure(1)
makeGraph(dieRolls, 'violet')
plt.show()