### Opis dataseta mtcars
Motor Trend Car Road Tests

The data was extracted from the 1974 Motor Trend US magazine, and comprises fuel consumption and 10 aspects of automobile design and performance for 32 automobiles (1973–74 models
* mpg - Miles/(US) gallon
* cyl - Number of cylinders
* disp - Displacement (cu.in.)
* hp - Gross horsepower
* drat - Rear axle ratio
* wt - Weight (lb/1000)
* qsec - 1/4 mile time
* vs - V/S
* am - Transmission (0 = automatic, 1 = manual)
* gear - Number of forward gears
* carb - Number of carburetors

Source: R built in dataset
------------------------------------------------------------
1.ZADATAK i 2.ZADATAK: potrebno je filtrirati email adrese pomoću re biblioteke regularnim izrazima. Zatim, potrebno je filtrirati email adrese na 5 načina definiranim u zadatku 2. Potrebno je dobro znati regularne izraze.
3.ZADATAK: potrebno je generirati nasumično količinu muškaraca i žena te izračunati njihove visine prema normalnoj Gaussovoj razdiobi te prikazati na grafu. Potrebno je koristiti matplotlib.pyplot i numpy normalnu razdiobu.
4.ZADATAK: potrebno je simulirati 100 bacanja kockice te prikazati na grafu rezultate, isto kao i srednju vrijednost.
5.ZADATAK: potrebno prikazati ovisnost konjskih snaga o potrošnji auta na temelju danih podataka (mtcars). Također, potrebno je prikazati i informaciju o težini pojedinog automobila. Trebaju se izdvojiti informacije iz CSV datoteke te ih prikazati.
6.ZADATAK: potrebno je povećati svjetlinu slike. Učitamo matricu slike preko matlotlib.image biblioteke te ju pomnožimo s brojevim većim od 1. Ako je umnožak veći od 1, koristimo funkciju np.clip kako bi se matrica mogla ponovno prikazati kao slika.
