# -*- coding: utf-8 -*-
"""
Created on Tue Nov 30 18:09:53 2021

@author: Bernarda
"""

import re

def makniAt(emailImena):
    imena=[]
    for ime in emailImena:
        imena.append(ime[0:ime.find('@')])
    return imena

fname = 'mbox-short.txt'
try:
    fhand = open(fname)
except:
    print ('Datoteka ', fname, ' se ne moze otvoriti.')
    exit()

listaImena=[]
for line in fhand:
    lista = re.findall(r'\S+@\S', line)
    for email in lista:
        ime = makniAt(lista)
        if(ime not in listaImena):
            listaImena.append(ime)

mailUsernames = ' '.join(str(e) for e in listaImena)

zadatak1 = re.findall(r'\b\S*[aA]+\S*\b', mailUsernames)
zadatak2 = re.findall(r'\b[^aA ]*[aA][^aA ]*\b', mailUsernames)
zadatak3 = re.findall(r'\b[^aA ]+\b', mailUsernames)
zadatak4 = re.findall(r'\b\S*[0-9]+\S*', mailUsernames)
zadatak5 = re.findall(r'\b[a-z]+\b', mailUsernames)

print("Prvi dio adresa do @ znaka: ", mailUsernames)
print("Sve adrese s najmanje jednim slovom a: ", zadatak1)
print("Sve adrese s jednim slovom a: ", zadatak2)
print("Sve adrese bez slova a: ", zadatak3)
print("Sve adrese s numeričkim znakovima: ", zadatak4)
print("Sve adrese s malim slovima: ", zadatak5)

