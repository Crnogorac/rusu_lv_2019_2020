# -*- coding: utf-8 -*-
"""
Created on Tue Nov 30 18:47:04 2021

@author: Bernarda
"""

import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np 


img = mpimg.imread(r"C:\Users\Bernarda\Documents\faks\rusu\rusu_lv_2019_2020\LV2\solutions\tiger.png")

plt.figure(1)
plt.imshow(img)

img *= 1.5
img = np.clip(img, 0, 1)

plt.figure(2)
plt.imshow(img)
