# -*- coding: utf-8 -*-
"""
Created on Fri Oct 29 16:05:15 2021

@author: Bernarda
"""
def total_kn(radni_sat, kn_h):
    return radni_sat*kn_h

radni_sati = float(input("Radni sat: "))
kn_h = float(input("kn/h: "))
ukupno = total_kn(radni_sati, kn_h)

print("Ukupno: ", ukupno, " kn")