# -*- coding: utf-8 -*-
"""
Created on Fri Oct 29 17:24:22 2021

@author: Bernarda
"""
brojevi = []
while(True):
    unos = input("Unesi broj: ")
    if(unos == "Done"):
        break
    
    try:
        brojevi.append(float(unos))
    except:
        print("Molim unesite broj!")
        continue
    
maxi = max(brojevi)
mini = min(brojevi)
average = sum(brojevi)/len(brojevi)

print("Unjeli ste: ", len(brojevi), "brojeva. \n Maksimum je: ", maxi,
      "\nMinimum je: ", mini,
      "\nSrednja vrijednost je: ", average)