# -*- coding: utf-8 -*-
"""
Created on Fri Oct 29 17:31:43 2021

@author: Bernarda
"""

fname = input("Ime datoteke: ")

try:
    fhand = open(fname)
except:
    print("Datoteku ", fname, " nije moguće otvoriti!")
    exit()

spamPouzdanost = []
for linija in fhand:
    rijeci = linija.split()
    
    for idx, rijec in enumerate(rijeci):
        if(rijec == "X-DSPAM-Confidence:"):
            spamPouzdanost.append(float(rijeci[idx + 1]))
            
average = sum(spamPouzdanost)/len(spamPouzdanost)
print("Average X-DSPAM-Confidence: ", average)