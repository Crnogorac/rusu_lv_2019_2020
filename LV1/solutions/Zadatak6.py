# -*- coding: utf-8 -*-
"""
Created on Fri Oct 29 17:41:42 2021

@author: Bernarda
"""

fname = input("Ime datoteke: ")
try:
    fhand = open(fname)
except:
    print("Datoteku ", fname, " nije moguce otvoriti")
    exit()

domene = dict()
mailovi = []

for linija in fhand:
    rijeci = linija.split()
    
    for idx, rijec in enumerate(rijeci):
        if(rijec == "From"):
            mail = rijeci[idx+1]
            mailovi.append(mail)
            domena = mail[mail.find('@')+1::]
            
            if(domena in domene):
                domene[domena] += 1
            else:
                domene[domena] = 1
i = 0
for domena in domene:
    print(domena)
    print(mailovi[i])
    i += 1
    if(i >= 3):
        break