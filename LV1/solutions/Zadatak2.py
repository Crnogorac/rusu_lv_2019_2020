# -*- coding: utf-8 -*-
"""
Created on Fri Oct 29 17:15:17 2021

@author: Bernarda
"""

def getGrade(ocjena):
    slovo='A'
    if(ocjena<0.6):
        slovo = 'F'
    elif(ocjena<0.7):
        slovo = 'D'
    elif(ocjena<0.8):
        slovo = 'C'
    elif(ocjena<0.9):
        slovo = 'B'
    else:
        slovo = 'A'
    return slovo

try:
    ocjena=float(input("Unesi ocjenu: "))
    
    if((ocjena<0.0) or (ocjena>1.0)):
        print("Ocjena van intervala")
    else:
        print(ocjena, getGrade(ocjena))
        
except:
    print("Ocjena nije broj")
