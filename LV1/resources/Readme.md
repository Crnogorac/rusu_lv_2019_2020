Ovaj repozitorij sadrži python skriptu koja broji koliko puta se pojedina riječ pojavljuje u tekstualnoj datatoteci. Rezultat bi trebao biti dictionary gdje uz svaku riječ (key) stoji broj ponavljanja riječi (value). Međutim, skripta ima bugove i ne radi kako je zamišljeno.
-----------------------------------------------
Potrebno je bilo: - dodati zagrade kod print funkcije,
				  - maknuti raw_ kod raw_input(),
				  - maknuti x kod fnamex (fname = open(fnamex),
				  - u else dodati + (else: counts[word] += 1) kako bi brojali riječi u slučaju da ih je više